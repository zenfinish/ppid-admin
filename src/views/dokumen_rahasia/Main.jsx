import React, { Component } from 'react';
import { Segment, Table, Button, Icon, Modal, Header, Image } from 'semantic-ui-react';

class Main extends Component {

	state = {
		loading: false,
		open: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Table color="yellow">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell colSpan="3">
								Daftar Dokumen Informasi Rahasia
							</Table.HeaderCell>
						</Table.Row>
						<Table.Row>
							<Table.HeaderCell colSpan="3">
								<Button basic color='yellow' onClick={() => {this.setState({ open: true })}}>
									<Icon name='add square' /> Tambah Data
								</Button>
							</Table.HeaderCell>
						</Table.Row>
						<Table.Row>
							<Table.HeaderCell>Food</Table.HeaderCell>
							<Table.HeaderCell>Calories</Table.HeaderCell>
							<Table.HeaderCell>Protein</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell>Apples</Table.Cell>
							<Table.Cell>200</Table.Cell>
							<Table.Cell>0g</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>Orange</Table.Cell>
							<Table.Cell>310</Table.Cell>
							<Table.Cell>0g</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
				<Modal open={this.state.open}  onClose={() => {this.setState({ open: false })}} dimmer="blurring">
					<Modal.Header>Tambah Dokumen Informasi Publik</Modal.Header>
					<Modal.Content image>
						<Image wrapped size='medium' src='https://react.semantic-ui.com/images/avatar/large/rachel.png' />
						<Modal.Description>
						<Header>Default Profile Image</Header>
						<p>
							We've found the following gravatar image associated with your e-mail
							address.
						</p>
						<p>Is it okay to use this photo?</p>
						</Modal.Description>
					</Modal.Content>
				</Modal>
			</Segment>
		)
	}
}

export default Main