import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';

class Dashboard extends Component {

	state = {
		loading: false,
		open: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				Selamat Datang.
			</Segment>
		)
	}
}

export default Dashboard