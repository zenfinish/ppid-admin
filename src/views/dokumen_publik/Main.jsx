import React, { Component } from 'react';
import { Segment, Table, Button, Icon, Modal, Form, Message, Select } from 'semantic-ui-react';
import api from '../../configs/api.js';

class Main extends Component {

	state = {
		loading: false,
		loadingSimpan: false,
		loadingDokumen: false,
		open: false,
		openMessage: 'none',

		dataGrup: [],
		dataSkpd: [],

		file: null,
		nama: '',
		keterangan: '',
		type: '',
		grup_id: '',
		skpd_id: '',

		listDokumen: [],
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			api.get(`/admin/skpd`)
			.then(result => {
				let dataSkpd = [];
				for (let i = 0; i < result.data.length; i++) {
					dataSkpd.push({ key: result.data[i].id, value: result.data[i].id, text: result.data[i].nama });
				}
				this.setState({ dataSkpd: dataSkpd }, () => {
					api.get(`/admin/grup`)
					.then(result => {
						let dataGrup = [];
						for (let i = 0; i < result.data.length; i++) {
							dataGrup.push({ key: result.data[i].id, value: result.data[i].id, text: result.data[i].nama });
						}
						this.setState({ dataGrup: dataGrup, loading: false });
					})
					.catch(error => {
						console.log(error.response);
						this.setState({ loading: false });
					});
				});
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
		this.fetchDokumen();
	}

	fetchDokumen = () => {
		this.setState({
			loadingDokumen: true
		}, () => {
			api.get(`/admin/dokumen`)
			.then(result => {
				this.setState({ listDokumen: result.data, loadingDokumen: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingDokumen: false });
			});
		});
	}

	simpan = () => {
		this.setState({
			loadingSimpan: true
		}, () => {
			const formData = new FormData();
			formData.append('file', this.state.file);
			formData.append('nama', this.state.nama);
			formData.append('keterangan', this.state.keterangan);
			formData.append('type', this.state.type);
			formData.append('jenis', '1');
			formData.append('grup_id', this.state.grup_id);
			formData.append('skpd_id', this.state.skpd_id);
			api.post(`/admin/dokumen`, formData, {
				headers: {
					'content-type': 'multipart/form-data'
				}
			})
			.then(result => {
				this.setState({ loadingSimpan: false, open: false });
				this.fetchDokumen();
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingSimpan: false });
			});
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Segment basic loading={this.state.loadingDokumen}>
					<Table color="yellow">
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell colSpan="7">
									Daftar Dokumen Informasi Publik
								</Table.HeaderCell>
							</Table.Row>
							<Table.Row>
								<Table.HeaderCell colSpan="7">
									<Button basic color='yellow' onClick={() => {this.setState({ open: true })}}>
										<Icon name='add square' /> Tambah Data
									</Button>
								</Table.HeaderCell>
							</Table.Row>
							<Table.Row>
								<Table.HeaderCell>Tgl</Table.HeaderCell>
								<Table.HeaderCell>Nama</Table.HeaderCell>
								<Table.HeaderCell>Keterangan</Table.HeaderCell>
								<Table.HeaderCell>type</Table.HeaderCell>
								<Table.HeaderCell>Group</Table.HeaderCell>
								<Table.HeaderCell>SKPD</Table.HeaderCell>
								<Table.HeaderCell>Action</Table.HeaderCell>
							</Table.Row>
						</Table.Header>
						<Table.Body>
							{
								this.state.listDokumen.map((item, index) => (
									<Table.Row key={index}>
										<Table.Cell>{item.tgl}</Table.Cell>
										<Table.Cell>{item.nama}</Table.Cell>
										<Table.Cell>{item.keterangan}</Table.Cell>
										<Table.Cell>{item.type}</Table.Cell>
										<Table.Cell>{item.grup_nama}</Table.Cell>
										<Table.Cell>{item.skpd_nama}</Table.Cell>
										<Table.Cell>
											<Button icon='download' />
											<Button icon='trash' />
										</Table.Cell>
									</Table.Row>
								))
							}
						</Table.Body>
					</Table>
				</Segment>
				<Modal open={this.state.open}  onClose={() => {this.setState({ open: false })}} dimmer="blurring" size="tiny">
					<Modal.Header>Tambah Dokumen Informasi Publik</Modal.Header>
					<Modal.Content>
						<Modal.Description>
							<Form>
								<Form.Field>
									<label>Upload Dokumen</label>
									<input
										type="file"
										onChange={
											(e) => {
												let nama = e.target.files[0].name.split('.');
												nama.pop();
												let nama2 = nama.join(' ');
												this.setState({
													file: e.target.files[0],
													nama: nama2,
													type: e.target.files[0].type,
												});
											}
										}
									/>
									<span style={{ color: 'grey', fontSize: 11 }}><i>Batas maksimal upload foto adalah</i> <b>2MB</b></span>
								</Form.Field>
								<Form.Field>
									<label>Nama Dokumen</label>
									<input type="text" onChange={(e) => {this.setState({ nama: e.target.value })}} value={this.state.nama} />
								</Form.Field>
								<Form.Field>
									<label>Keterangan</label>
									<textarea
										onChange={(e) => {this.setState({ keterangan: e.target.value })}}
										value={this.state.keterangan}
									/>
								</Form.Field>
								<Form.Field>
									<label>Group</label>
									<Select
										placeholder='Select Group' options={this.state.dataGrup}
										onChange={
											(e, { value }) => {
												this.setState({ grup_id: value })
											}
										}
									/>
								</Form.Field>
								<Form.Field>
									<label>SKPD</label>
									<Select placeholder='Select SKPD' options={this.state.dataSkpd}
										onChange={(e, { value }) => {this.setState({ skpd_id: value })}}
									/>
								</Form.Field>
								<Form.Field>
									<Button loading={this.state.loadingSimpan} onClick={this.simpan}>Simpan</Button>
								</Form.Field>
								<Form.Field>
									<Message negative style={{ display: this.state.openMessage }}>
										<Message.Header>{this.state.headerMessage}</Message.Header>
										<p>{this.state.pMessage}</p>
									</Message>
								</Form.Field>
							</Form>
						</Modal.Description>
					</Modal.Content>
				</Modal>
			</Segment>
		)
	}
}

export default Main
