import React, { Component } from 'react';
import { Segment, Table } from 'semantic-ui-react';

class Main extends Component {

	state = {
		loading: false,
		open: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Table color="yellow">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell colSpan="3">
								Daftar Permohonan Informasi
							</Table.HeaderCell>
						</Table.Row>
						<Table.Row>
							<Table.HeaderCell>Food</Table.HeaderCell>
							<Table.HeaderCell>Calories</Table.HeaderCell>
							<Table.HeaderCell>Protein</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell>Apples</Table.Cell>
							<Table.Cell>200</Table.Cell>
							<Table.Cell>0g</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>Orange</Table.Cell>
							<Table.Cell>310</Table.Cell>
							<Table.Cell>0g</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
			</Segment>
		)
	}
}

export default Main