import React, { Component } from 'react';
import { Container, Grid, Header, Segment } from 'semantic-ui-react';

class Footer extends Component {

	render() {
		return (
			<Segment inverted vertical style={{ padding: '5em 0em' }} color="yellow">
				<Container>
					<Grid divided inverted stackable>
						<Grid.Row>
							<Grid.Column width={16}>
								<Header inverted as='h4' content='Ⓒ PPID Kota Bandar Lampung' />
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Container>
			</Segment>
		)
	}
}

export default Footer