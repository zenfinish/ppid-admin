import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch, withRouter } from 'react-router-dom';
import { Segment } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import api from './configs/api.js';
import Login from './Login.jsx';
import Main from './Main.jsx';

class App extends Component {
	
	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			api.get(`/admin/cek-token`)
			.then(result => {
				this.setState({ loading: false });
			})
			.catch(error => {
				this.setState({ loading: false });
				this.props.history.push('/');
			});
		});
	}
		
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Router basename={process.env.REACT_APP_BASENAME}>
					<Switch>
						<Route exact path={`${process.env.PUBLIC_URL}/`} component={Login} />
						<Route path={`${process.env.PUBLIC_URL}/main`} component={Main} />
					</Switch>
				</Router>
			</Segment>
		)
	}
}

export default withRouter(App);