import React, { Component } from 'react'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react';
import api from './configs/api.js';

class Login extends Component {

	state = {
		email: '',
		password: '',
	}

	login = (e) => {
		e.preventDefault();
		api.post(`/admin/login`, {
			...this.state
		})
		.then(result => {
			localStorage.setItem('token', result.data);
			this.props.history.push('/main');
		})
		.catch(error => {
			console.log(error.response);
		});
	}
	
	render() {
		return (
			<Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
				<Grid.Column style={{ maxWidth: 450 }}>
					<Header as='h2' color='teal'><Image src={require('./img/LOGO_KOTA_BANDAR_LAMPUNG_BARU.png')} /> Log-in Admin PPID</Header>
					<Form size='large' onSubmit={this.login}>
						<Segment stacked>
							<Form.Input
								fluid icon='user'
								iconPosition='left'
								placeholder='E-mail address'
								autoComplete="off"
								onChange={(e) => {this.setState({ email: e.target.value })}}
							/>
							<Form.Input
								fluid
								icon='lock'
								iconPosition='left'
								placeholder='Password'
								type='password'
								autoComplete="off"
								onChange={(e) => {this.setState({ password: e.target.value })}}
							/>
							<Button color='teal' fluid size='large'>
								Login
							</Button>
						</Segment>
					</Form>
					<Message>Hubungi Administrator Untuk Pembuatan Akun</Message>
				</Grid.Column>
			</Grid>
		)
	}

}

export default Login;
