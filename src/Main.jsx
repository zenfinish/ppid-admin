import React, { Component } from 'react';
import { Button, Image, Menu, Segment, Sidebar, Icon, Container } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import Footer from './Footer.jsx';
import { HashRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Dashboard from './views/Dashboard.jsx';
import PermohonanInformasi from './views/permohonan_informasi/Main.jsx';
import DokumenPublik from './views/dokumen_publik/Main.jsx';
import DokumenRahasia from './views/dokumen_rahasia/Main.jsx';

export default class Main extends Component {
	
	state = {
		animation: 'push',
		direction: 'left',
		dimmed: false,
		visible: false,
		activeIndex: 0,
	}

	handleAnimationChange = (animation) => () =>
		this.setState((prevState) => ({ animation, visible: !prevState.visible }))

	handleDimmedChange = (e, { checked }) => this.setState({ dimmed: checked })

	handleDirectionChange = (direction) => () =>
		this.setState({ direction, visible: false })

	handleClick = (e, titleProps) => {
		const { index } = titleProps;
		const { activeIndex } = this.state;
		const newIndex = activeIndex === index ? -1 : index;
		this.setState({ activeIndex: newIndex })
	}

	render() {
		const { direction } = this.state;
		const vertical = direction === 'bottom' || direction === 'top';

		return (
			<div>
				<Sidebar.Pushable as={Segment} basic color="yellow">
					{vertical ? null : (
						<Sidebar
							as={Segment}
							animation={this.state.animation}
							direction={this.state.direction}
							basic
							vertical
							visible={this.state.visible}
						>
							<Menu vertical fluid secondary>
								<Menu.Item
									active={this.state.activeIndex === 1}
									onClick={(e, { name }) => this.setState({ activeItem: name })}
									as={Link}
									to={`${process.env.PUBLIC_URL}/main/permohonan-informasi`}
								><Icon name='info circle' /> Permohonan Informasi</Menu.Item>
								<Menu.Item
									active={this.state.activeIndex === 1}
									onClick={(e, { name }) => this.setState({ activeItem: name })}
									as={Link}
									to={`${process.env.PUBLIC_URL}/main/dokumen-publik`}
								><Icon name='address card outline' /> Dokumen Informasi Publik</Menu.Item>
								<Menu.Item
									active={this.state.activeIndex === 1}
									onClick={(e, { name }) => this.setState({ activeItem: name })}
									as={Link}
									to={`${process.env.PUBLIC_URL}/main/dokumen-rahasia`}
								><Icon name='address card' /> Dokumen Informasi Rahasia</Menu.Item>
		  					</Menu>
						</Sidebar>
					)}

					<Sidebar.Pusher>
						<Segment basic inverted style={{ backgroundColor: 'white', padding: '0em' }}>
							<Segment
								inverted
								vertical
								style={{ padding: '1em' }}
								color="yellow"
							>
								<Button
									circular icon='sidebar'
									onClick={this.handleAnimationChange('slide out')}
								></Button>
								<Container style={{ float: 'right', textAlign: 'right' }}>
									<Button
										onClick={this.handleAnimationChange('slide out')}
										style={{ padding: '0em' }}
										circular
									><Image src={require('./img/LOGO_KOTA_BANDAR_LAMPUNG_BARU.png')} size="mini" /></Button>
								</Container>
							</Segment>
							<Router basename={process.env.REACT_APP_BASENAME}>
								<Switch>
									<Route exact path={`${process.env.PUBLIC_URL}/main/`} component={Dashboard} />
									<Route path={`${process.env.PUBLIC_URL}/main/permohonan-informasi`} component={PermohonanInformasi} />
									<Route path={`${process.env.PUBLIC_URL}/main/dokumen-publik`} component={DokumenPublik} />
									<Route path={`${process.env.PUBLIC_URL}/main/dokumen-rahasia`} component={DokumenRahasia} />
								</Switch>
							</Router>
							<Footer />
						</Segment>
					</Sidebar.Pusher>
				</Sidebar.Pushable>
			</div>
		)
	}

}